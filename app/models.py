from datetime import date

from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from import_export import resources

from mptt.models import MPTTModel, TreeForeignKey

from .tasks import send_email_task
from . import managers


class User(AbstractUser):
    username = None
    department = models.CharField(max_length=100, default="department name")
    email = models.EmailField(_('email address'), unique=True)
    grant_access_to_this_account = models.BooleanField(default=False)
    notify_date = models.DateField(null=True, blank=True)
    expiration_date = models.DateField(null=True, blank=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = managers.UserManager()

    def clean(self):
        if self.expiration_date and self.notify_date:
            if self.expiration_date < self.notify_date:
                raise ValidationError('Notify time should be before send time')
        return super().clean()

    def save_base(self, raw=False, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.notify_date != '' and self.notify_date is not None:
            days = (self.expiration_date - self.notify_date).days
            send_email_task.apply_async(args=[self.username,
                                              self.email,
                                              self.expiration_date,
                                              days
                                              ], countdown=days * 86400)
        if (self.notify_date == '' or self.notify_date is None) and self.expiration_date:
            days = (self.expiration_date - date.today()).days
            send_email_task.apply_async(args=[self.username,
                                              self.email,
                                              self.expiration_date,
                                              days
                                              ], countdown=days * 86400)
        super().save_base(raw, force_insert, force_update, using, update_fields)


class Category(MPTTModel):
    name = models.CharField(max_length=200)
    parent = TreeForeignKey(to='self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    # created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Item(models.Model):
    id = models.CharField(max_length=200, primary_key=True)
    name = models.CharField(max_length=200)
    rank = models.IntegerField(null=True, blank=True)
    quantity = models.IntegerField(null=True, blank=True)
    verified = models.BooleanField(null=True, blank=True)
    category = models.ManyToManyField(Category, blank=True, related_name='item')
    # updated_at = models.DateTimeField(auto_now=True)
    # created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['rank']


class Sample(models.Model):
    id = models.CharField(max_length=200, primary_key=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    date = models.DateField()
    name = models.CharField(max_length=200)
    text = models.CharField(max_length=200)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Favourite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    added_at = models.DateTimeField(auto_now=True)
    comment = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['-added_at']

    def __str__(self):
        return f'{ self.item.name } -> { self.user.email }'
