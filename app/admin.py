from django.contrib import admin
import csv
from django.http import HttpResponse
from mptt.admin import DraggableMPTTAdmin
from django.contrib.auth.models import Group
from import_export.admin import ImportExportModelAdmin

from . import models, resources

admin.site.unregister(Group)


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    fields = (
        'first_name', 'last_name', 'email', 'department', 'grant_access_to_this_account', 'notify_date',
        'expiration_date'
    )
    list_display = ('email', 'first_name', 'grant_access_to_this_account')


@admin.register(models.Favourite)
class FavouriteAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Category, DraggableMPTTAdmin)


@admin.register(models.Item)
class ItemAdmin(ImportExportModelAdmin):
    fields = ('id', 'name', 'rank', 'quantity', 'verified', 'category')
    list_display = ('name', 'rank', 'quantity', 'verified')
    resource_class = resources.ItemResource


@admin.register(models.Sample)
class SampleAdmin(ImportExportModelAdmin):
    fields = ('id', 'item', 'date', 'name', 'text')
    list_display = ('id', 'name', 'text')
