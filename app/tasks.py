from celery import shared_task
from django.core.mail import send_mail

from django.template.loader import render_to_string

from project import settings
import time


@shared_task
def sleepy(duration):
    time.sleep(duration)
    return None


@shared_task
def send_email_task(first_name, email, send_at, expire_period):
    subject = 'Update your account'
    message = render_to_string('upgrade-account.html', {
        'first_name': first_name,
        'expire_on': send_at,  # expires on
        'day': expire_period,
    })
    send_mail(subject, message, settings.EMAIL_HOST_USER, [email])
    return "Success"
