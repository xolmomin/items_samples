from import_export import resources, fields, widgets

from . import models


class ItemResource(resources.ModelResource):
    parent = fields.Field(column_name='parent', attribute='parent', widget=widgets.ForeignKeyWidget(models.Item, 'name'))

    class Meta:
        model = models.Item
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ('id',)
        fields = (
            'id',
            'name',
            'rank',
            'quantity',
            'verified',
            'category',
            # 'updated_at',
            # 'created_at',
        )

    def dehydrate_category(self, parent):
        cats = parent.category.all()
        cat_string = ''

        def build_to_root(cat):
            if cat.parent:
                return f"{ build_to_root(cat.parent) }.{ cat.name }"
            else:
                return f"{ cat.name }"

        for cat in cats:
            cat_string += f'{ build_to_root(cat) }; '

        return cat_string[:-2]

    @staticmethod
    def build_categories(category_path):

        item_cats = []

        for cats in category_path.split('; '):
            parent = None

            for i, cat in enumerate(cats.split('.')):
                try:
                    parent = models.Category.objects.get(name=cat)
                except models.Category.MultipleObjectsReturned:
                    break
                except models.Category.DoesNotExist:
                    kws = {"name": cat}

                    if parent:
                        kws['parent'] = parent

                    parent = models.Category.objects.create(**kws)
                    parent.save()

            item_cats.append(parent)

        return item_cats

    def before_import_row(self, row, **kwargs):
        if row['category']:
            cats = self.build_categories(row['category'])
            categories = ",".join([str(cat.id) for cat in cats])
            row['category'] = categories
