from django import forms
from django.core.exceptions import ValidationError

from app.models import User, Favourite


class SignUpForm(forms.Form):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    email = forms.EmailField(max_length=100)
    department = forms.CharField(max_length=100)
    password1 = forms.CharField(max_length=100)
    password2 = forms.CharField(max_length=100)

    def clean_password1(self):
        password1 = self.data.get('password1')
        password2 = self.data.get('password2')
        if password1 != password2:
            raise ValidationError('Passwords did not match')

    def save(self):
        user = User.objects.create_user(
            email=self.data.get('email'),
            first_name=self.data.get('first_name'),
            last_name=self.data.get('last_name'),
            department=self.data.get('department'),
            password=self.data.get('password1')
        )
        return user


class SignInForm(forms.Form):
    email = forms.EmailField(max_length=100)
    password = forms.CharField(max_length=100)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not User.objects.filter(email=email).exists():
            raise ValidationError('Email is not registered !')

    def clean_password(self):
        email = self.data.get('email')
        password = self.data.get('password')
        user = User.objects.filter(email=email).first()
        if user:
            if not user.check_password(password):
                raise ValidationError('Password is incorrect!')


class UserProfileForm(forms.Form):
    first_name = forms.CharField(max_length=100, required=False)
    last_name = forms.CharField(max_length=100, required=False)
    department = forms.CharField(max_length=100, required=False)


class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(max_length=100,)
    new_password = forms.CharField(max_length=100,)
    confirm_password = forms.CharField(max_length=100,)


class FavouriteItemForm(forms.ModelForm):
    class Meta:
        model = Favourite
        fields = [
            'item',
            'comment',
            'user',
        ]
