from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about_page, name='about'),
    path('signup', views.signup_page, name='signup_page'),
    path('signin', views.signin_page, name='signin_page'),
    path('activate/<str:uidb64>/<str:token>', views.activate, name='activate'),
    path('logout', views.logout_page, name='logout_page'),
]

urlpatterns += [
    path('account', views.account, name='account'),
    path('account/delete-account', views.delete_account, name='delete_account'),
    path('account/search_items', views.item_page, name='item'),
    path('account/profile_item/<str:id>', views.profile_item_page, name='profile_item'),
    path('account/favorite_items', views.favorite_items_page, name='favorite_items'),
]

urlpatterns += [
    path('change_password/', views.change_password, name='change_password'),
    # path('reset_password/', auth_views.PasswordResetView.as_view(template_name="reset-password/reset_password.html"), name='reset_password'),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name="reset-password/password_reset_sent.html"), name='password_reset_done'),
    path('reset/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(template_name="reset-password/password_reset_form.html"), name='password_reset_confirm'),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name="reset-password/password_reset_done.html"), name='password_reset_complete')
]
