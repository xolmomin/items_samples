from django import template


register = template.Library()


@register.filter(name='get_favourites')
def get_favourites(favourite_set):
    return [item.item for item in favourite_set]


@register.filter(name='display_cats')
def display_cats(cats_set):
    return ", ".join([item.name for item in cats_set.all()])
