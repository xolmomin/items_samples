import datetime

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.core.mail import send_mail
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib import messages
from django.template.loader import render_to_string
from django.core import serializers
from .forms import UserProfileForm
from project import settings

from app.models import User
from app.tokens import account_activation_token

from app.forms import SignUpForm, SignInForm, FavouriteItemForm, ChangePasswordForm
from .tasks import send_email_task
from . import models


def index(request):
    return render(request, 'index.html')


def about_page(request):
    return render(request, 'about.html')


def signup_page(request):
    form = SignUpForm()

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            if not User.objects.filter(email=form.cleaned_data['email']).count():
                user = form.save()
                subject = 'Activate your account'
                current_site = get_current_site(request)
                message = render_to_string('activation-account.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                send_mail(subject, message, settings.EMAIL_HOST_USER, [form.data.get('email')])
                messages.add_message(
                    request=request,
                    level=messages.SUCCESS,
                    message="Please confirm your email, to activate your account"
                )

            else:
                messages.add_message(
                    request=request,
                    level=messages.ERROR,
                    message="Sorry, this mail already exists"
                )
        else:
            for msg in form.errors:
                messages.add_message(
                    request=request,
                    level=messages.ERROR,
                    message=form.errors[msg][0]
                )
    return render(request, 'signup-page.html', {'form': form})


def signin_page(request):
    if request.method == 'POST':
        form = SignInForm(request.POST)
        if form.is_valid():
            user = User.objects.filter(email=form.data.get('email')).first()
            if user:
                login(request, user)
                return redirect('index')
        else:
            return render(request, 'signin-page.html', {'form': form})
    return render(request, 'signin-page.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        messages.add_message(
            request=request,
            level=messages.SUCCESS,
            message="Your account successfully activated!"
        )
        return redirect('index')
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')


@login_required
def account(request):
    form = UserProfileForm()

    if request.method == 'POST':
        form = UserProfileForm(request.POST)
        if form.is_valid():
            user = User.objects.get(pk=request.user.pk)
            user.first_name = form.data.get('first_name')
            user.last_name = form.data.get('last_name')
            user.department = form.data.get('department')
            user.save()

    return render(request, 'user/account.html', {'form': form})


@login_required
def item_page(request):
    if request.method == 'POST':

        form = FavouriteItemForm(request.POST)
        form.user = request.user

        if form.is_valid():
            form.save()

    if request.user.grant_access_to_this_account and (
            (datetime.date.today() < request.user.expiration_date) if request.user.expiration_date else False
    ):

        qs = models.Item.objects.all()

        c = {
            "min": qs.filter(quantity__isnull=False).order_by('quantity')[0].quantity if qs.filter(quantity__isnull=False).order_by('quantity').count() else 0,
            "max": qs.filter(quantity__isnull=False).order_by('quantity').reverse()[0].quantity if qs.filter(quantity__isnull=False).order_by('quantity').reverse().count() else 0,
            'favourite_form': FavouriteItemForm()
        }

        params = list(request.GET.keys())

        if 'min' in params and (not request.GET['min'] == ''):
            qs = qs.filter(quantity__gte=request.GET['min'])

        if 'max' in params and (not request.GET['max'] == ''):
            qs = qs.filter(quantity__lte=request.GET['max'])

        if 'verified' in params and request.GET['verified'] == 'on':
            qs = qs.filter(verified=True)

        if 'items_samples' in params and request.GET['items_samples'] == 'on':
            qs = qs.filter(sample__isnull=False)

        if request.GET.get('category') is not None:
            filter_category = request.GET.get('category')
            if filter_category != '':
                filter_categories = filter_category.split(', ')
                category = models.Category.objects.filter(name__in=filter_categories)
                qs = qs.filter(category__in=category).distinct()

        c['items'] = qs.distinct()
        c['has_access'] = True

        page = request.GET.get('page', 1)

        paginator = Paginator(c['items'], 20)
        try:
            c['items'] = paginator.page(page)
        except PageNotAnInteger:
            c['items'] = paginator.page(1)
        except EmptyPage:
            c['items'] = paginator.page(paginator.num_pages)
    else:
        c = {'has_access': False}

    categories = models.Category.objects.all()
    c['categories'] = categories
    # {'page_obj': page_obj}
    return render(request, 'user/tables.html', c)


@login_required
def profile_item_page(request, id):
    if request.method == 'POST':

        form = FavouriteItemForm(request.POST)
        form.user = request.user

        if form.is_valid():
            form.save()

    if request.user.grant_access_to_this_account and (
            (datetime.date.today() < request.user.expiration_date) if request.user.expiration_date else False
    ):

        c = {
            'favourite_form': FavouriteItemForm(),
            'has_access': True,
            'item': models.Item.objects.get(id=id),
        }

    else:
        c = {'has_access': False}

    return render(request, 'user/profile_items.html', c)


def favorite_items_page(request):
    if request.method == "POST":
        data = request.POST
        if data['action'] == 'remove_comment':
            if request.user.favourite_set.filter(item_id=data['item']).count():
                request.user.favourite_set.filter(item_id=data['item']).first().delete()

    c = {
        'items': request.user.favourite_set.all(),
        "has_access": request.user.grant_access_to_this_account
    }
    return render(request, 'user/table_favorite.html', c)


def reset_password(request):
    return render(request, 'reset-password.html')


def logout_page(request):
    logout(request)
    return redirect('index')


@login_required
def delete_account(request):
    u = request.user
    logout(request)
    models.User.objects.get(pk=u.pk).delete()
    messages.add_message(
        request=request,
        level=messages.SUCCESS,
        message="Your account successfully deleted"
    )
    return render(request, 'index.html')


@login_required
def change_password(request):
    c = {}

    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)

        user = models.User.objects.get(pk=request.user.pk)

        if form.is_valid():
            if user.check_password(form.cleaned_data['current_password']):
                if form.cleaned_data['new_password'] == form.cleaned_data['confirm_password']:
                    user.set_password(form.cleaned_data['confirm_password'])
                    user.save()

                    messages.add_message(
                        request=request,
                        level=messages.SUCCESS,
                        message="Your password has been changed"
                    )
                    return redirect('signin_page')
                else:
                    messages.add_message(
                        request=request,
                        level=messages.ERROR,
                        message="Passwords didn't match"
                    )
            else:
                messages.add_message(
                    request=request,
                    level=messages.ERROR,
                    message="Current password is incorrect"
                )
        else:
            messages.add_message(
                request=request,
                level=messages.ERROR,
                message="Sorry, please try again!"
            )
    else:
        c['form'] = ChangePasswordForm()

    return render(request, 'user/change_password.html', c)
