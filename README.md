Instructions to setup the project.

Setup:
1. The first thing to do is to clone the repository from gitlab with the following command: 
    ```
    git clone https://gitlab.com/afzalyusupov/items_samples.git
    ```

2. Open the directory, which you cloned from gitlab in your preferred IDE.

3. After opening the directory in your IDE, run the following commands to install all required dependencies including Celery and RabbitMQ (see the full Instructions: https://docs.celeryproject.org/en/stable/getting-started/first-steps-with-celery.html): 
    ```
    pip install -r requirements.txt
    pip install celery
    sudo apt-get install rabbitmq-server
    ```

4. Once pip has finished downloading the dependencies, run the following commands in your terminal:
    ```
    python manage.py makemigrations
    python manage.py migrate
    python manage.py createsuperuser
    ```

5. To start the local server run the following command: 
    ```
    python manage.py runserver
    ```

6. Open your browser and navigate to http://127.0.0.1:8000/

7. To access the admin panel, navigate to http://127.0.0.1:8000/admin in your browser.


P. S. 
1. Don't forget to switch DEBUG variable to False in settings.py file.
2. Celery and RabbitMQ are required in both development and production mode.
